# Pandemics recipe: default HTML

This repo contains the default Pandemics recipe for HTML format.
The styling of this recipe is coming almost entirely from the [paper Jekyll theme](https://github.com/mkchoi212/paper-jekyll-theme) (Mike JS. Choi), which is also used for the official [Pandemics website](pandemics.gitlab.io).

As this the default format for HTML format, you just need to specify that you want to use the HTML format to apply this recipe:

```
---
pandemics:
  format: html
---
```

If you want to explicitly reference this recipe, eg. to use an older version, just add in the YAML front-matter of your markdown document:

```
---
pandemics:
  recipe: gitlab.com:pandemics/recipe-default-html SHA

---
```

where `SHA` is the commit number of the version you want to use.
